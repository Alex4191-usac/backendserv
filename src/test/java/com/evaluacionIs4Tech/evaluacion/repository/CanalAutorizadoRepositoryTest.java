package com.evaluacionIs4Tech.evaluacion.repository;

import com.evaluacionIs4Tech.evaluacion.bo.CanalAutorizado;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DataJpaTest
class CanalAutorizadoRepositoryTest {

    @Autowired
    private CanalAutorizadoRepository underTest;

    @AfterEach
    void tearDown(){
        underTest.deleteAll();
    }


    @Test
    void itShouldCheckIfCanalExists() {
        //given
        CanalAutorizado canal = new CanalAutorizado();
        canal.setNombreCanal("Canal 1");
        underTest.save(canal);

        //when
        Long n = underTest.ExisteCanal(1L);

        //then
        assertThat(n).isGreaterThan(0L);
    }

}
package com.evaluacionIs4Tech.evaluacion.bo;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Getter
@Setter
@Table(
        name="producto"
)
public class Producto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="codigo_producto")
    private Long codigoP;
    private String descripcion;
    private Double monto;
    @ManyToOne
    @JoinColumn(name="codigo_distribuidor")
    private Distribuidor distribuidor;

}

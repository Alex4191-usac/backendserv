package com.evaluacionIs4Tech.evaluacion.bo;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Getter
@Setter
@Table(
        name="distribuidor"
)
public class Distribuidor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="codigo_distribuidor")
    private Long codigo;
    @Column(name="nombre_distribuidor")
    private String nombre;
    @Column(name="correo_notificacion")
    private String cNotificacion;
    @Column(name="correo_alerta")
    private String cAlerta;
    @ManyToOne
    @JoinColumn(name = "codigo_canal")
    private CanalAutorizado canalAutorizado;

}

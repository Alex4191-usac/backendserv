package com.evaluacionIs4Tech.evaluacion.bo;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@Data
@Entity
@Getter
@Setter
@Table(
        name="canal_autorizado"
)
public class CanalAutorizado {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="codigo_canal")
    private Long codigoCanal;
    @Column(name="nombre_canal", nullable = false)
    private String nombreCanal;


}

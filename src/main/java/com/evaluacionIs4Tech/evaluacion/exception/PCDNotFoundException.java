package com.evaluacionIs4Tech.evaluacion.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class PCDNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public PCDNotFoundException(String message){
        super(message);
    }
}

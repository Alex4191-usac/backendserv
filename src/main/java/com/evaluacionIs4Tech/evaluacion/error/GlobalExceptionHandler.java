package com.evaluacionIs4Tech.evaluacion.error;


import com.evaluacionIs4Tech.evaluacion.exception.PCDNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZoneId;
import java.time.ZonedDateTime;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(PCDNotFoundException.class)
    public ResponseEntity<?> handleUserNotFoundException(
            PCDNotFoundException exception) {
        ExceptionDetails exceptionD = new ExceptionDetails(
                exception.getMessage(),
                HttpStatus.NOT_FOUND,
                ZonedDateTime.now(ZoneId.of("Z"))
        );
        return new ResponseEntity<>(exceptionD, exceptionD.getHttpStatus());

    }
}

package com.evaluacionIs4Tech.evaluacion.service;
import com.evaluacionIs4Tech.evaluacion.dto.DistribuidorDTO;

import java.util.List;

public interface DistribuidorService {
    public List<DistribuidorDTO> listaCanales();
    public String guardar(DistribuidorDTO distribuidorDTO);
    public void eliminar(Long codigoDistribuidor);
    public String actualizar(DistribuidorDTO distribuidorDTO, Long codigoD);
    public DistribuidorDTO obtenerDistribuidor(Long codigoD);
}

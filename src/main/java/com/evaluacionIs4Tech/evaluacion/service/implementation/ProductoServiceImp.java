package com.evaluacionIs4Tech.evaluacion.service.implementation;
import com.evaluacionIs4Tech.evaluacion.bo.Distribuidor;
import com.evaluacionIs4Tech.evaluacion.bo.Producto;
import com.evaluacionIs4Tech.evaluacion.dto.DistribuidorDTO;
import com.evaluacionIs4Tech.evaluacion.dto.ProductoDTO;
import com.evaluacionIs4Tech.evaluacion.exception.PCDNotFoundException;
import com.evaluacionIs4Tech.evaluacion.repository.ProductoRepository;
import com.evaluacionIs4Tech.evaluacion.service.ProductoService;
import com.evaluacionIs4Tech.evaluacion.util.ConvertEntityToDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
@Service
public class ProductoServiceImp implements ProductoService {
    @Autowired
    private ProductoRepository productoRepository;
    @Autowired
    private ConvertEntityToDTO convertEntityToDTO;

    @Override
    public List<ProductoDTO> listaProducto() {
        List<ProductoDTO> listaDto = null;
        List<Producto> listaProducto = productoRepository.findAll();
        return listaDto = listaProducto.stream().map(param->convertEntityToDTO.convertToProducto(param))
                .collect(Collectors.toList());
    }

    @Override
    public String guardar(ProductoDTO productoDTO) {
        Producto p = new Producto();
        p.setDescripcion(productoDTO.getDescripcion());
        p.setMonto(productoDTO.getMonto());
        p.setDistribuidor(productoDTO.getDistribuidor());
        productoRepository.save(p);
        return "INSERTADO";
    }

    @Override
    public void eliminar(Long codigoProducto) {
        productoRepository.deleteById(codigoProducto);
    }

    @Override
    public String actualizar(ProductoDTO productoDTO, Long codigoP) {
            Long p = productoRepository.ExisteProducto(codigoP);
            if(p>0){
                Producto distro = productoRepository.obtenerDistri(codigoP);
                distro.setDescripcion(productoDTO.getDescripcion());
                distro.setMonto(productoDTO.getMonto());
                distro.setDistribuidor(productoDTO.getDistribuidor());
                productoRepository.save(distro);
            }else{
                throw new PCDNotFoundException("se intento acceder a un recurso no existente");
            }
            return "Actualizado";

    }

    @Override
    public ProductoDTO obtenerProducto(Long codigoP) {
        Producto p = this.productoRepository.findById(codigoP)
                .orElseThrow(()-> new PCDNotFoundException("Producto no existente"));
        return convertEntityToDTO.convertToProducto(p);

    }
}

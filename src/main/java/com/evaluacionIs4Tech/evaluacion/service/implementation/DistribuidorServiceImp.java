package com.evaluacionIs4Tech.evaluacion.service.implementation;

import com.evaluacionIs4Tech.evaluacion.bo.CanalAutorizado;
import com.evaluacionIs4Tech.evaluacion.bo.Distribuidor;
import com.evaluacionIs4Tech.evaluacion.dto.DistribuidorDTO;
import com.evaluacionIs4Tech.evaluacion.exception.PCDNotFoundException;
import com.evaluacionIs4Tech.evaluacion.repository.DistribuidorRepository;
import com.evaluacionIs4Tech.evaluacion.service.DistribuidorService;
import com.evaluacionIs4Tech.evaluacion.util.ConvertEntityToDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
@Service
public class DistribuidorServiceImp implements DistribuidorService {
    @Autowired
    private DistribuidorRepository distribuidorRepository;
    @Autowired
    private ConvertEntityToDTO convertEntityToDTO;


    @Override
    public List<DistribuidorDTO> listaCanales() {
        List<DistribuidorDTO> listaDto = null;
        List<Distribuidor> listaDistro = distribuidorRepository.findAll();
        return listaDto = listaDistro.stream().map(param->convertEntityToDTO.convertToDistribuidor(param))
                .collect(Collectors.toList());
    }

    @Override
    public String guardar(DistribuidorDTO distribuidorDTO) {
        Distribuidor d = new Distribuidor();
        d.setNombre(distribuidorDTO.getNombre());
        d.setCNotificacion(distribuidorDTO.getCNotificacion());
        d.setCAlerta(distribuidorDTO.getCAlerta());
        d.setCanalAutorizado(distribuidorDTO.getCanalAutorizado());
        distribuidorRepository.save(d);
        return "INSERTADO";
    }

    @Override
    public void eliminar(Long codigoDistribuidor) {
        distribuidorRepository.deleteById(codigoDistribuidor);

    }

    @Override
    public String actualizar(DistribuidorDTO distribuidorDTO, Long codigoD) {

        Long d = distribuidorRepository.ExisteDistribuidor(codigoD);

        if(d>0){
            Distribuidor distro = distribuidorRepository.obtenerDistri(codigoD);

            distro.setNombre(distribuidorDTO.getNombre());
            distro.setCAlerta(distribuidorDTO.getCAlerta());
            distro.setCNotificacion(distribuidorDTO.getCNotificacion());
            distro.setCanalAutorizado(distribuidorDTO.getCanalAutorizado());
            distribuidorRepository.save(distro);
        }else{
            throw new PCDNotFoundException("se intento acceder a un recurso no existente");
        }
        return "Actualizado";
    }




    public DistribuidorDTO obtenerDistribuidor(Long codigoD) {
        Distribuidor d = this.distribuidorRepository.findById(codigoD)
                .orElseThrow(()-> new PCDNotFoundException("El usuario con el id:"+codigoD+" no existe"));
        return convertEntityToDTO.convertToDistribuidor(d);
    }





}

package com.evaluacionIs4Tech.evaluacion.service.implementation;

import com.evaluacionIs4Tech.evaluacion.bo.CanalAutorizado;
import com.evaluacionIs4Tech.evaluacion.dto.CanalAutorizadoDTO;
import com.evaluacionIs4Tech.evaluacion.exception.PCDNotFoundException;
import com.evaluacionIs4Tech.evaluacion.repository.CanalAutorizadoRepository;
import com.evaluacionIs4Tech.evaluacion.service.CanalAutorizadoService;

import com.evaluacionIs4Tech.evaluacion.util.ConvertEntityToDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CanalAutorizadoServiceImp implements CanalAutorizadoService {
    @Autowired
    private CanalAutorizadoRepository canalAutorizadoRepository;
    @Autowired
    private ConvertEntityToDTO convertEntityToDTO;


    @Override
    public List<CanalAutorizadoDTO> listaCanales() {
        List<CanalAutorizadoDTO> listaDto = null;
        List<CanalAutorizado> listaCanal = canalAutorizadoRepository.findAll();
        return listaDto = listaCanal.stream().map(param->convertEntityToDTO.convertToCanal(param))
                .collect(Collectors.toList());
    }

    @Override
    public String guardar(CanalAutorizadoDTO canalAutorizadoDTO) {
        CanalAutorizado canal = new CanalAutorizado();
        canal.setNombreCanal(canalAutorizadoDTO.getNombreCanal());
        canalAutorizadoRepository.save(canal);
        return "OK";
    }

    @Override
    public void eliminar(Long codigoCanal) {
        canalAutorizadoRepository.deleteById(codigoCanal);
    }

    @Override
    public String actualizarCanal(CanalAutorizadoDTO canalAutorizadoDTO, Long codigoC) {

        Long c = canalAutorizadoRepository.ExisteCanal(codigoC);

        if(c>0){
            CanalAutorizado canal = new CanalAutorizado();
            BeanUtils.copyProperties(canalAutorizadoDTO,canal);
            canalAutorizadoRepository.save(canal);
            return "SUCCESS";
        }else{
            throw new PCDNotFoundException("se intento acceder a un recurso no existente");
        }

    }




}

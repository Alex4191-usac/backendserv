package com.evaluacionIs4Tech.evaluacion.service;

import com.evaluacionIs4Tech.evaluacion.dto.ProductoDTO;

import java.util.List;

public interface ProductoService {
    public List<ProductoDTO> listaProducto();
    public String guardar(ProductoDTO productoDTO);
    public void eliminar(Long codigoProducto);
    public String actualizar(ProductoDTO productoDTO, Long codigoP);
    public ProductoDTO obtenerProducto(Long codigoP);
}

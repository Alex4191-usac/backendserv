package com.evaluacionIs4Tech.evaluacion.service;

import com.evaluacionIs4Tech.evaluacion.dto.CanalAutorizadoDTO;

import java.util.List;

public interface CanalAutorizadoService {
    public List<CanalAutorizadoDTO> listaCanales();
    public String guardar(CanalAutorizadoDTO canalAutorizadoDTO);
    public void eliminar(Long codigoCanal);
    public String actualizarCanal(CanalAutorizadoDTO canalAutorizadoDTO,Long codigoC);
    // public CanalAutorizadoDTO obtenerCanal(Long codigoCanal);


}

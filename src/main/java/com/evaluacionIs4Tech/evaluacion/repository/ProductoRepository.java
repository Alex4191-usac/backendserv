package com.evaluacionIs4Tech.evaluacion.repository;

import com.evaluacionIs4Tech.evaluacion.bo.Producto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductoRepository extends JpaRepository<Producto, Long> {

    @Query(value = "SELECT count(*) FROM producto WHERE codigo_producto =:codigoP",nativeQuery = true)
    long ExisteProducto(@Param("codigoP") Long codigoP);

    @Query(value = "SELECT * FROM producto WHERE  codigo_producto =:idProducto",nativeQuery = true)
    Producto obtenerDistri(@Param("idProducto") Long idProducto);

}

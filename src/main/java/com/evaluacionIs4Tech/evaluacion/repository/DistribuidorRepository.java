package com.evaluacionIs4Tech.evaluacion.repository;

import com.evaluacionIs4Tech.evaluacion.bo.Distribuidor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DistribuidorRepository extends JpaRepository<Distribuidor,Long> {

    @Query(value = "SELECT count(*) FROM distribuidor WHERE codigo_distribuidor =:codigoD",nativeQuery = true)
    long ExisteDistribuidor(@Param("codigoD") Long codigoD);

    @Query(value = "SELECT * FROM distribuidor WHERE  codigo_distribuidor =:idDist",nativeQuery = true)
    Distribuidor obtenerDistri(@Param("idDist") Long idDist);
}

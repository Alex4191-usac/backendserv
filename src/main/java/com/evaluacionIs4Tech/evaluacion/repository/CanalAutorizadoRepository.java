package com.evaluacionIs4Tech.evaluacion.repository;

import com.evaluacionIs4Tech.evaluacion.bo.CanalAutorizado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CanalAutorizadoRepository extends JpaRepository<CanalAutorizado, Long> {


    @Query(value = "SELECT count(*) FROM canal_autorizado WHERE codigo_canal =:codigoC", nativeQuery = true)
    long ExisteCanal(@Param("codigoC") Long codigoC);

}

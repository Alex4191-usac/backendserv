package com.evaluacionIs4Tech.evaluacion.dto;



import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
@Getter
@Setter
public class CanalAutorizadoDTO implements Serializable {
    private static final long serialVersionUID = 3L;
    private Long codigoCanal;
    private String nombreCanal;

}

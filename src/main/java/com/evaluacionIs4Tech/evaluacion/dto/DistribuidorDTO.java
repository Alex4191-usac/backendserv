package com.evaluacionIs4Tech.evaluacion.dto;

import com.evaluacionIs4Tech.evaluacion.bo.CanalAutorizado;
import com.evaluacionIs4Tech.evaluacion.bo.Distribuidor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

@Getter
@Setter
public class DistribuidorDTO {
    private static final long serialVersionUID = 2L;
    private Long codigo;
    private String nombre;
    private String cNotificacion;
    private String cAlerta;
    private CanalAutorizado canalAutorizado;



    /*public DistribuidorDTO(Distribuidor d ){
        BeanUtils.copyProperties(d, this);
    }*/

}

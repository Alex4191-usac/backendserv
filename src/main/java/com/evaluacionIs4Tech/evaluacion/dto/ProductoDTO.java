package com.evaluacionIs4Tech.evaluacion.dto;

import com.evaluacionIs4Tech.evaluacion.bo.Distribuidor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductoDTO {
    private Long codigoP;
    private String descripcion;
    private Double monto;
    private Distribuidor distribuidor;
}

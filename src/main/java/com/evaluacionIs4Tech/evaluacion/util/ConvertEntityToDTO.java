package com.evaluacionIs4Tech.evaluacion.util;

import com.evaluacionIs4Tech.evaluacion.bo.CanalAutorizado;
import com.evaluacionIs4Tech.evaluacion.bo.Distribuidor;
import com.evaluacionIs4Tech.evaluacion.bo.Producto;
import com.evaluacionIs4Tech.evaluacion.dto.CanalAutorizadoDTO;
import com.evaluacionIs4Tech.evaluacion.dto.DistribuidorDTO;
import com.evaluacionIs4Tech.evaluacion.dto.ProductoDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("entityToDTO")
public class ConvertEntityToDTO {

    @Autowired
    private ModelMapper modelMapper;

    public CanalAutorizadoDTO convertToCanal(CanalAutorizado canalAutorizado){
        CanalAutorizadoDTO canalDTO = null;
        if(canalAutorizado!= null){
            canalDTO = modelMapper.map(canalAutorizado, CanalAutorizadoDTO.class);
        }
        return canalDTO;
    }


    public ProductoDTO convertToProducto(Producto producto){
        ProductoDTO pDTO  = null;
        if(producto!= null){
            pDTO = modelMapper.map(producto, ProductoDTO.class);
        }
        return pDTO;
    }

    public DistribuidorDTO convertToDistribuidor(Distribuidor distribuidor){
        DistribuidorDTO distiDTO = null;
        if(distribuidor!=null){
            distiDTO=modelMapper.map(distribuidor,DistribuidorDTO.class);
        }
        return distiDTO;
    }


}

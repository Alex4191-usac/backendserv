package com.evaluacionIs4Tech.evaluacion.controller;
import com.evaluacionIs4Tech.evaluacion.dto.DistribuidorDTO;
import com.evaluacionIs4Tech.evaluacion.service.DistribuidorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/distribuidores")
public class DistribuidorController {
    @Autowired
    private DistribuidorService distribuidorService;

    @PostMapping
    public String save(@RequestBody DistribuidorDTO distribuidorDTO) {
        distribuidorService.guardar(distribuidorDTO);
        return "inserted";
    }

    @DeleteMapping("{id}")
    public void deleteByCodigo(@PathVariable Long id) {
        this.distribuidorService.eliminar(id);
    }

    @GetMapping
    public List<DistribuidorDTO> findAll(){
        return distribuidorService.listaCanales();
    }

    @PutMapping(path = "/update/{codigoD}")
    public String actualizarCanal(@RequestBody DistribuidorDTO distribuidorDTO, @PathVariable Long codigoD){
        return distribuidorService.actualizar(distribuidorDTO,codigoD);
    }

    @GetMapping("{id}")
    public DistribuidorDTO obtenerByCodigo(@PathVariable Long id) {
      return this.distribuidorService.obtenerDistribuidor(id);
    }



}

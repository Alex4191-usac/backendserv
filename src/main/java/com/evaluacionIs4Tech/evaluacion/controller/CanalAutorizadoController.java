package com.evaluacionIs4Tech.evaluacion.controller;

import com.evaluacionIs4Tech.evaluacion.dto.CanalAutorizadoDTO;
import com.evaluacionIs4Tech.evaluacion.service.CanalAutorizadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/canales")
public class CanalAutorizadoController {
    @Autowired
    private CanalAutorizadoService canalAutorizadoService;


    @PostMapping
    public String save(@RequestBody CanalAutorizadoDTO canalAutorizadoDTO) {
        canalAutorizadoService.guardar(canalAutorizadoDTO);
        return "inserted";
    }

    @DeleteMapping("{id}")
    public void deleteByCodigo(@PathVariable Long id) {
        this.canalAutorizadoService.eliminar(id);
    }

    @GetMapping
    public List<CanalAutorizadoDTO> findAll(){
        return canalAutorizadoService.listaCanales();
    }

    @PutMapping(path = "/actualizar/{codigoC}")
    public String actualizarCanal(@RequestBody CanalAutorizadoDTO canalAutorizadoDTO, @PathVariable Long codigoC){
        return  canalAutorizadoService.actualizarCanal(canalAutorizadoDTO,codigoC);
    }

}

package com.evaluacionIs4Tech.evaluacion.controller;

import com.evaluacionIs4Tech.evaluacion.dto.DistribuidorDTO;
import com.evaluacionIs4Tech.evaluacion.dto.ProductoDTO;
import com.evaluacionIs4Tech.evaluacion.repository.ProductoRepository;
import com.evaluacionIs4Tech.evaluacion.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
@RestController
@RequestMapping(value = "api/productos", produces = APPLICATION_JSON_VALUE)
public class ProductoController {
    @Autowired
    private ProductoService productoService;


    @GetMapping
    public List<ProductoDTO> findAll(){
        return productoService.listaProducto();
    }

    @PostMapping
    public String save(@RequestBody ProductoDTO productoDTO) {
        productoService.guardar(productoDTO);
        return "inserted";
    }

    @DeleteMapping("{id}")
    public void deleteByProducto(@PathVariable Long id) {
        this.productoService.eliminar(id);
    }

    @PutMapping(path = "/update/{codigoP}")
    public String actualizarProducto(@RequestBody ProductoDTO productoDTO, @PathVariable Long codigoP){
        return productoService.actualizar(productoDTO,codigoP);
    }

    @GetMapping("{id}")
    public ProductoDTO obtenerByCodigo(@PathVariable Long id) {
        return this.productoService.obtenerProducto(id);
    }

}
 /*







*/